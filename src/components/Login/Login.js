import './Login.css';
import {getAuth, signInAnonymously,} from 'firebase/auth';
import { Link } from 'react-router-dom';
import {Button} from 'antd'

const Login = () => {
    const login = () => {
        signInAnonymously(getAuth()).then((usuario)=>console.log(usuario))
    }


    return(
        <div className='container'>
            <h1>Iniciar sesion</h1>
            <Button type='primary' onClick={login}>
                <Link to='/Home'>Login</Link>
            </Button>

        </div>
    );
}

export default Login;