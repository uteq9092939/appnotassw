import '../App.css';
import '../styles/global.css';
import { useEffect, useState } from 'react';
import { Modal, FloatButton, Input, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import NotesList from '../components/NotesList';
import Contador from '../components/Contador';
import BuscadorNotas from '../components/BuscadorNotas';
import { useNavigate } from 'react-router-dom';
import { getToken,onMessage } from 'firebase/messaging';
import { messaging } from '../firebase';
import {ToastContainer,toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const { TextArea } = Input;

function Home() {
    //Usar navigate('/ruta del app')
    const navigate = useNavigate();
  const rootStyles = getComputedStyle(document.documentElement);
  const [searchTerm, setSearchTerm] = useState('');
  const handleSearchChange = (value) => {
    setSearchTerm(value);
  };

  const colorPrimario = rootStyles.getPropertyValue('--color-primario');
  const colorLight = rootStyles.getPropertyValue('--color-light');
  const colorFondo = rootStyles.getPropertyValue('--color-fondo');
  const colorSecundario = rootStyles.getPropertyValue('--color-secundario');
  const color4 = rootStyles.getPropertyValue('--color4');
  const color5 = rootStyles.getPropertyValue('--color5');

  const [modalAddNote, setModalAddNote] = useState(false);
  const [noteText, setNoteText] = useState('');
  const [notesList, setNotesList] = useState([]);
  // const filteredNotes = notesList.filter((note) =>
  //   note.note.toLowerCase().includes(searchTerm.toLowerCase())
  // );
  const [validations, setValidations] = useState({});

  const [contadorNotasCompletadas, setContadorNotasCompletadas] = useState(0);

  // context para alertas ant design
  const [success, contextHolder] = message.useMessage();

  const getTokenNotification=async ()=>{
    const token= await getToken(messaging,{
      vapidKey:'BF76LJAvMcPJToI9A4U18VBiS8OYzLB6aNENgvQJ8SVAdWAWEimWQcHEbG6jbJtoFIkkoItohvD9KVQxCBie0Dk'
    }).catch((err) => console.log('No se puede obtener el token',err))
    if (token){
      console.log('Token:',token)
    }if(!token){
      console.log('No hay token disponible')
    }
  }
const notificarme=()=>{
  if(!window.Notification){
    console.log('Este navegador no soporta notificaciones');
    return;
  }
  if(Notification.permission==='granted'){
    getTokenNotification();//Obtener y moestrar el token en la consola
  }else if(Notification.permission!=='denied'|| Notification.permission==='default'){
    Notification.requestPermission((permission)=>{
      console.log(permission);
      if(permission==='granted'){
        getTokenNotification();//Obtener y mostrar el token en la consola
      }
    })
  }
}
notificarme();
useEffect(()=>{
  getTokenNotification()
  onMessage(messaging,message=>{
    console.log('onMessage',message)
    toast(message.notification.title)
  })
},[])





  const handleChangeInput = (txt) => {
    setNoteText(txt.target.value);
  };

  /**
   * Función para mostrar y reiniciar valores de texto de input
   */
  const handleShowModal = () => {
    setNoteText('');
    setValidations({});
    setModalAddNote(!modalAddNote);
  };

  /**
   * Función para validar y guardar nota
   */
  const saveNote = () => {
    // Validamos que el texto no esté vacío o tenga saltos de línea
    if (noteText.trim() !== '') {
      const id = new Date().getTime().toString();
      const newNote = { id: id, note: noteText, noteDone: false };
      // console.log(newNote)
      // Realizamos la solicitud HTTP POST para guardar la nota
      fetch('http://localhost:3001/api/note', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newNote),
      })
        .then((response) => {
          if (response.status === 200) {
            
            setNotesList([...notesList, newNote]);
            setModalAddNote(false);
            successAlert();
            setValidations({});
          } else {
            // Manejar errores de la API aquí si es necesario
            console.error('Error al guardar la nota');
          }
        })
        .catch((error) => {
          console.error('Error en la solicitud HTTP:', error);
        });
    } else {
      setValidations({
        ...validations,
        inpNote: 'El texto de la nota no puede estar vacío.',
      });
    }
  };


  const getNotes = () => {
    // Realizamos la solicitud HTTP GET para obtener notas
    fetch('http://localhost:3001/api/note', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status === 200) {
          // Parsea la respuesta como JSON y luego guárdala en setNotesList
          response.json()
            .then((data) => {
              console.log(data);
            })
            .catch((jsonError) => {
              console.error('Error al parsear la respuesta JSON:', jsonError);
            });
        } else {
          // Manejar errores de la API aquí si es necesario
          console.error('Error al obtener las notas');
        }
      })
      .catch((error) => {
        console.error('Error en la solicitud HTTP:', error);
      });
  };

  const completarNota = (noteId) => {
    const newNotes = notesList.map((note) => {
      if (note.id === noteId) {
        setContadorNotasCompletadas(contadorNotasCompletadas + 1);
        return { ...note, noteDone: true };
      }
      return note;
    });
    completedAlert();
    setNotesList(newNotes);
  };

  const successAlert = () => {
    success.open({
      type: 'success',
      content: 'Nota guardada correctamente',
      duration: 6,
    });
  };

  const completedAlert = () => {
    success.open({
      type: 'success',
      content: 'Nota finalizada',
      duration: 2,
    });
  };

  useEffect(() => {
    let notesStorage = localStorage.getItem('notes');
    if (notesStorage) {
      setNotesList(JSON.parse(notesStorage));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('notes', JSON.stringify(notesList));
  }, [notesList]);
  const eliminarNota = (noteId) => {
    const noteToDelete = notesList.find((note) => note.id === noteId);
    if (noteToDelete && noteToDelete.noteDone) {
      setContadorNotasCompletadas(contadorNotasCompletadas - 1);
    }
    const newNotes = notesList.filter((note) => note.id !== noteId);
    setNotesList(newNotes);
  };
  return (
    <div className="App">
      <h1>Aplicación para crear notas</h1>
      <ToastContainer/>
      
      {/*CONTADOR */}
      <Contador contadorNotasCompletadas={contadorNotasCompletadas} />
      {/* Buscador */}
      <BuscadorNotas onSearchChange={handleSearchChange} />

      <button onClick={getNotes()}>GET notas de MongoDB</button>
      {/* Lista de notas */}
      <NotesList
        notes={notesList}
        completarNota={completarNota}
        eliminarNota={eliminarNota}
      />

      {/* Modal agregar nota */}
      <Modal
        title="Agregar nota"
        centered
        open={modalAddNote}
        onOk={() => saveNote()}
        okText="Guardar"
        onCancel={() => setModalAddNote(false)}
        cancelText="Cancelar"
        okButtonProps={{
          style: {
            background: colorPrimario,
            borderColor: colorPrimario,
            color: colorLight,
          },
        }}
      >
        <TextArea
          value={noteText}
          onChange={handleChangeInput}
          rows={3}
          placeholder="Escribe una nota..."
        />
        {'inpNote' in validations && (
          <p className="error-text">{validations.inpNote}</p>
        )}
      </Modal>

      {/* Botón flotante mostrar modal */}
      <FloatButton
        onClick={() => handleShowModal()}
        type="secondary"
        icon={<PlusOutlined />}
        size="large"
        style={{
          backgroundColor: colorPrimario,
          color: colorLight,
        }}
      />

      


      {/* Renderizamos context alert */}
      {contextHolder}
    </div>
  );
}

export default Home;
