// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getMessaging, getToken} from "firebase/messaging";
const firebaseConfig = {
  apiKey: "AIzaSyDDmzHGWHw1eCqaDW4EWgFDP5AT6dk08Vc",
  authDomain: "test-push-cfb48.firebaseapp.com",
  projectId: "test-push-cfb48",
  storageBucket: "test-push-cfb48.appspot.com",
  messagingSenderId: "489869255844",
  appId: "1:489869255844:web:4dc2a7947502978e04b231",
  measurementId: "G-7Q4QL5BZ0X"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const messaging = getMessaging(app);