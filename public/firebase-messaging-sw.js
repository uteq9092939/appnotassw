importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js');

const firebaseConfig = {
  apiKey: "AIzaSyDDmzHGWHw1eCqaDW4EWgFDP5AT6dk08Vc",
  authDomain: "test-push-cfb48.firebaseapp.com",
  projectId: "test-push-cfb48",
  storageBucket: "test-push-cfb48.appspot.com",
  messagingSenderId: "489869255844",
  appId: "1:489869255844:web:4dc2a7947502978e04b231",
  measurementId: "G-7Q4QL5BZ0X"
};

const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage(payload => {
  console.log('Recibiendo mensaje en segundo plano')
  const tituloNotificacion = payload.notification.title;
  const options = {
    body: payload.notification.body,
    icon: './img/40.png',
  }

  self.registration.showNotification(tituloNotificacion, options);
})